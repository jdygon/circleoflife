﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	public GameObject gamePanel;
	public GameObject checkPanel;
	public GameObject listPanel;
	public GameObject scorePanel;
	public GameObject personPrefab;

	private const int DESCRIPTION_TEXT = 0;
	private const int MAX_CYCLES = 7;
	private const int MAX_PERSONS = 10;

	private GameObject[] persons = new GameObject[MAX_PERSONS];
	private Person activePerson;
	private int cycle;

	void Awake() {
		gamePanel.SetActive(false);
		checkPanel.SetActive(false);
		scorePanel.SetActive(false);
		Resources.LoadLifeStories(Age.Senior);
	}

	public void StartGame() {
		gamePanel.SetActive(true);
		InstantiatePersons();
		cycle = 0;
		RunCycle();
	}

	void InstantiatePersons() {
		Resources.LoadPersons();
		for (int i = 0; i < MAX_PERSONS; i++) {
			int tempInt = i;
			string name = Resources.persons[i].name;
			persons[i] = Instantiate(personPrefab) as GameObject;
			persons[i].name = "Person " + (i+1);
			persons[i].GetComponent<Person>().firstName = name;
			persons[i].GetComponent<Person>().attributes = new Attributes(); //TODO is that good? instantiate Prefab field here
			persons[i].GetComponentInChildren<Text>().name = name;
			persons[i].GetComponent<Person>().gender = Resources.persons[i].gender;
			persons[i].GetComponent<Person>().button.GetComponentInChildren<Text>().text = name;
			persons[i].GetComponent<Person>().button.onClick.AddListener(() => CheckPerson(persons[tempInt].GetComponent<Person>()));
			persons[i].transform.SetParent(listPanel.transform);
		}
	}

	private void RunCycle()	{
		SetPersonsInteractable(true);
		if (!EndOfCycles())
			ExpandPersonsLifeTime();
		else
			ShowGameScore();
	}

	private bool EndOfCycles() {
		return cycle >= MAX_CYCLES;
	}

	public void CheckPerson(Person person) {
		activePerson = person;
		checkPanel.SetActive(true);
		SetPersonsInteractable(false);
		checkPanel.GetComponentInChildren<Text>().text = person.LoadInfo((Age)cycle);
		//vs checkPanel.GetComponentsInChildren<Text>()[DESCRIPTION_TEXT].text = person.LoadInfo();
	}

	public void KillPerson() {
		checkPanel.SetActive(false);
		activePerson.Die();
		//TODO show future of dead person
		RunCycle();
	}

	public void SavePerson() {
		checkPanel.SetActive(false);
		activePerson = null;
		RunCycle();
	}

	private void ExpandPersonsLifeTime() {
		foreach (GameObject person in persons) {
			LifeStory newLifeStory = Resources.ChooseRandomStory((Age)cycle);
			person.GetComponent<Person>().lifeTime.Add(newLifeStory.description);
			person.GetComponent<Person>().attributes.Modify(newLifeStory.attributes);
		}
		cycle++;
	}

	private void SetPersonsInteractable(bool toogle) {
		foreach (GameObject person in persons)
			if (person.GetComponent<Person>().isAlive)
				person.GetComponent<Button>().interactable = toogle;
	}

	private void ShowGameScore() {
		checkPanel.SetActive(false);
		scorePanel.SetActive(true);
	}
}
