﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Person : MonoBehaviour {
	public string firstName;
	public Gender gender;
	public bool isAlive;
	public bool isMarried;
	public Person marriedTo;
	public Attributes attributes;
	public List<string> lifeTime;
	public Button button;

	public Person(string name = "", Gender gen = Gender.Male) {
		firstName = name;
		gender = gen;
		isAlive = true;
		isMarried = false;
		marriedTo = null;
		attributes = new Attributes();
		lifeTime = new List<string>();
	}

	public string LoadInfo(Age present) {
		string description = ("Imie: " + firstName + " Płeć: " + gender.ToString("g")); //TODO angielskie nazwy + polskie :/

		if (isMarried)
			description += " Partner(ka): " + marriedTo.GetComponent<Person>().firstName;
		for (Age i = 0; i < present; i++)
			description += "\n" + i.ToString() + ": " + lifeTime[(int)i]; //TODO angielskie nazwy + polskie :/

		return description;
	}

	public void Die() {
		isAlive = false;
		button.interactable = false;
		button.GetComponentInChildren<Text>().color = new Color(0,0,0);
	}
}
