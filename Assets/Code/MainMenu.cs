﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	[SerializeField]
	private Button start;
	[SerializeField]
	private Button settings;
	[SerializeField]
	private Button quit;
	public GameObject mainMenuPanel;
	public GameController gameController;

	public void StartGame() {
		mainMenuPanel.SetActive(false);
		gameController.StartGame();
	}

	public void StartSettings() {
		//TODO
	}

	public void Quit() {
		//TODO
	}

	public void SetGameControllerReference(GameController controller) {
		gameController = controller;
	}
}
