﻿using System.Collections.Generic;
using System;

public enum Age {
	Child = 0,
	Kid,
	Teenager,
	Youth,
	Adult,
	MiddleAge,
	Senior
}

public enum Gender {
	Male,
	Female
}

public enum AttrName {
	Psyche = 0,
	Health,
	Eduaction
}

public class Attributes {
	private int statsNumber = Enum.GetNames(typeof(AttrName)).Length;
	public Dictionary<AttrName, int> stat = new Dictionary<AttrName, int>();

	public Attributes(int[] statsList = null) {
		for (int i = 0; i < statsNumber; i++) {
			int value = (statsList != null && statsList.Length >= statsNumber) ? statsList[i] : 0;
			stat.Add((AttrName)i, value);
		}
	}

	public void Modify(Attributes modifier) {
		for (int i = 0; i < statsNumber; i++)
			stat[(AttrName)i] += modifier.stat[(AttrName)i];
	}
}

public class LifeStory {
	public string description;
	public Attributes attributes;

	public LifeStory(string description, Attributes attributes) {
		this.description = description;
		this.attributes = attributes;
	}
}