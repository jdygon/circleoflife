﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System;

public class parsedPerson {
	public string name;
	public Gender gender;

	public parsedPerson(string n, string g) {
		name = n;
		gender = (Gender)Enum.Parse(typeof(Gender), g);
	}
}

public static class Resources { //TODO REFACTORE LoadLifeStories()

	public static List<parsedPerson> persons = new List<parsedPerson>();
	public static List<LifeStory> []lifeStories;
	private static string personsPath = Application.dataPath + "/SetupData/persons.json";
	private static string lifeStoriesPath = Application.dataPath + "/SetupData/lifeStories.json";
	private static readonly System.Random random = new System.Random();

	public static void LoadPersons() {
		string jsonString = File.ReadAllText(personsPath);
		JsonData itemData = JsonMapper.ToObject(jsonString);

		for (int i = 0; i < itemData["persons"].Count; i++)
			persons.Add(new parsedPerson(itemData["persons"][i][0].ToString(), itemData["persons"][i][1].ToString()));
	}

	public static void LoadLifeStories(Age cycles) {
		lifeStories = new List<LifeStory>[Enum.GetNames(typeof(Age)).Length];
		string jsonString = File.ReadAllText(lifeStoriesPath);
		JsonData itemData = JsonMapper.ToObject(jsonString);

		for (Age cycle = 0; cycle <= cycles; cycle++) {
			lifeStories[(int)cycle] = new List<LifeStory>();

			for (int i = 0; i < itemData["lifeCycles"][(int)cycle][0].Count; i++) {
				Attributes loadedAttribute = new Attributes(new int[] {
					(int)itemData["lifeCycles"][(int)cycle][0][i]["attributes"][0],
					(int)itemData["lifeCycles"][(int)cycle][0][i]["attributes"][1],
					(int)itemData["lifeCycles"][(int)cycle][0][i]["attributes"][2]
				});
				lifeStories[(int)cycle].Add(new LifeStory(itemData["lifeCycles"][(int)cycle][0][i]["description"].ToString(), loadedAttribute));
			}
		}
	}

	public static LifeStory ChooseRandomStory(Age cycle) {
		int rand = random.Next(0, 3);   //TODO RANDOM random.Next(range)
		Debug.Log("rand: " + rand);	//check if random is really random
		return lifeStories[(int)cycle][rand];
	}
	//TODO GetName, GetGender or just leave public???
}
